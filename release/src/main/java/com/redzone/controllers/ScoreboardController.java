package com.redzone.controllers;


import com.redzone.exception.ResourceNotFoundException;
import com.redzone.models.Scoreboard;
import com.redzone.repositories.ScoreboardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/scoreboard")
public class ScoreboardController {

    @Autowired
    ScoreboardRepository scoreboardRepository;


    public Integer getMaxScore() {
        return scoreboardRepository.getMaxScore();
    }

    @GetMapping("/topofthemonth")
    public ResponseEntity<Scoreboard> getUserWithMaxScore()
    {
        Scoreboard userWithMaxScore = scoreboardRepository.findByScore(getMaxScore()).orElseThrow(() -> new ResourceNotFoundException("User do not exist!"));
        return ResponseEntity.ok(userWithMaxScore);
    }

    @GetMapping("/getall")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public List<Scoreboard> getAll() {
        return scoreboardRepository.findTop10ByOrderByScoreDesc();
    }
}
