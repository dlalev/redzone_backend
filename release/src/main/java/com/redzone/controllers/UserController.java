package com.redzone.controllers;

import com.redzone.exception.ResourceNotFoundException;
import com.redzone.models.*;
import com.redzone.repositories.RoleRepository;
import com.redzone.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    private String userNotFound = "User with id: ";
    private String doNotExist = " do not exist";

    @GetMapping("/getall")
    public String allAccess() {
        return "Public.";
    }

    @GetMapping("/user")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String userAccess() {
        return "Choose which game to play";
    }


    @GetMapping("/admin")
    @PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
    public List<User> adminAccess() {
        return userRepository.findAll();
    }

    @GetMapping("/admin/user/{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
    public ResponseEntity<User> getUserById(@PathVariable Long id){
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(userNotFound + id + doNotExist));
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/admin/user/{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
    public ResponseEntity<Map<String, Boolean>> deleteUser(@PathVariable Long id){
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(userNotFound + id + doNotExist));

        String rolenotfound = "Error: Role is not found.";
        Role moderatorRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                .orElseThrow(() -> new RuntimeException(rolenotfound));
        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                .orElseThrow(() -> new RuntimeException(rolenotfound));


        if(user.getRoles().contains(moderatorRole) || user.getRoles().contains(adminRole)){

            Map<String, Boolean> response = new HashMap<>();
            response.put("moderator or admin", Boolean.FALSE);
            return ResponseEntity.ok(response);

        }
        else
        {
            userRepository.delete(user);
            Map<String, Boolean> response = new HashMap<>();
            response.put("deleted", Boolean.TRUE);
            return ResponseEntity.ok(response);
        }




    }

    @PutMapping("/admin/updateuser/{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
    public ResponseEntity<User> updateUser(@PathVariable Long id,@RequestBody UserDTO userDetails){
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(userNotFound + id + doNotExist));

        user.setUsername(userDetails.getUsername());
        user.setEmail(userDetails.getEmail());

        User updatedUser = userRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }

    @PutMapping("/increasescore/{id}/{score}")
    public ResponseEntity<User> updateScore(@PathVariable Long id, @PathVariable Integer score){
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(userNotFound + id + doNotExist));

        Integer currentScore = user.getScore();
        Integer newScore = score;
        Integer finalScore = currentScore + newScore;

        user.setScore(finalScore);

        User updatedUser = userRepository.save(user);
        return ResponseEntity.ok(updatedUser);
    }

    @PutMapping("/updateRole/{id}/{role}")
    public ResponseEntity<User> updateRole(@PathVariable Long id, @PathVariable ERole role){
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(userNotFound + id + doNotExist));


        Set<Role> oldroles;
        oldroles = user.getRoles();

        Set<Role> newroles = new HashSet<>();
        Role userRole = roleRepository.findByName(role)
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        newroles.add(userRole);
        if(oldroles != newroles){
                user.setRoles(newroles);
                userRepository.save(user);
                return ResponseEntity.ok(user);
        }
        else
        {
            return ResponseEntity.ok(user);
        }

    }




}