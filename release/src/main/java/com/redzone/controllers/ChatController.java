package com.redzone.controllers;

import com.redzone.models.ChatMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class ChatController {

    @MessageMapping("/user-all")
    @SendTo("/topic/user")
    public ChatMessage sendToAll(@Payload ChatMessage message) {
        return message;
    }


}
