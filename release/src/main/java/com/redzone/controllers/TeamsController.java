package com.redzone.controllers;


import com.redzone.exception.ResourceNotFoundException;
import com.redzone.models.ETeam;
import com.redzone.models.Teams;
import com.redzone.models.User;
import com.redzone.repositories.TeamsRepository;
import com.redzone.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class TeamsController {

    @Autowired
    private TeamsRepository teamRepository;

    @Autowired
    private UserRepository userRepository;

    private String userNotFound = "User with id: ";
    private String doNotExist = " do not exist";

    @GetMapping("/getAllTeams")
    public List<Teams> getAll()
    {

        List<Teams> all = teamRepository.findAll();
        return all;
    }

    @GetMapping("/blueteamm")
    public List<Teams> getAllfromBlueTeam()
    {
        ETeam blue = ETeam.BLUE;
        List<Teams> blueTeam = teamRepository.findByTeamNameLike(blue);
        return blueTeam;
    }

    @GetMapping("/redteamm")
    public List<Teams> getAllfromRedTeam()
    {
        ETeam red = ETeam.RED;
        List<Teams> redTeam = teamRepository.findByTeamNameLike(red);
        return redTeam;
    }



    @PutMapping("/addtoblue/{id}")
    public Teams addToBlueTeam(@PathVariable Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(userNotFound + id + doNotExist));
        ETeam blue = ETeam.BLUE;
        List<Teams> blueTeam = teamRepository.findByTeamNameLike(blue);
        if (blueTeam.contains(user)){
            return  null;
        }
        else
        {
            Teams team = new Teams(blue);
            List<User> users = new ArrayList<>();
            users.add(user);
            team.setUsers(users);
            blueTeam.add(team);
            teamRepository.delete(team);
            teamRepository.save(team);

            return team;
        }

    }

    @PutMapping("/addtored/{id}")
    public Teams addToRedTeam(@PathVariable Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(userNotFound + id + doNotExist));
        ETeam red = ETeam.RED;
        List<Teams> redTeam = teamRepository.findByTeamNameLike(red);
        if (redTeam.contains(user)){
            return  null;
        }
        else
        {
            Teams team = new Teams(red);
            List<User> users = new ArrayList<>();
            users.add(user);
            team.setUsers(users);
            redTeam.add(team);
            teamRepository.delete(team);
            teamRepository.save(team);
            return team;
        }

    }





}
