package com.redzone.repositories;

import com.redzone.models.Scoreboard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ScoreboardRepository extends JpaRepository<Scoreboard, Long> {

    @Query(value = "SELECT MAX(score) FROM users", nativeQuery = true)
    Integer getMaxScore();


    Optional<Scoreboard> findByScore(int score);

    List<Scoreboard> findTop10ByOrderByScoreDesc();
}
