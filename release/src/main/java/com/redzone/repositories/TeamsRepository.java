package com.redzone.repositories;

import com.redzone.models.ETeam;
import com.redzone.models.Teams;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeamsRepository  extends JpaRepository<Teams, Long> {

    List<Teams> findByTeamNameLike(ETeam team);

    void deleteById(Long id);

}
