package com.redzone.repositories;

import java.util.Optional;

import com.redzone.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    //User findByResetPasswordToken(String token);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}