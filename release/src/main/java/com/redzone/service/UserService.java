//package com.redzone.service;
//
//import com.redzone.exception.ResourceNotFoundException;
//import com.redzone.models.User;
//import com.redzone.repositories.UserRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Service;
//
//import javax.transaction.Transactional;
//
//@Service
//@Transactional
//public class UserService {
//
//    @Autowired
//    private UserRepository customerRepo;
//
//    public void updateResetPasswordToken(String token, String email) throws ResourceNotFoundException {
//        User customer = customerRepo.findByEmail(email);
//        if (customer != null) {
//            customer.setResetPasswordToken(token);
//            customerRepo.save(customer);
//        } else {
//            throw new ResourceNotFoundException("Could not find any customer with the email " + email);
//        }
//    }
//
//    public User getByResetPasswordToken(String token) {
//        return customerRepo.findByResetPasswordToken(token);
//    }
//
//    public void updatePassword(User customer, String newPassword) {
//        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//        String encodedPassword = passwordEncoder.encode(newPassword);
//        customer.setPassword(encodedPassword);
//
//        customer.setResetPasswordToken(null);
//        customerRepo.save(customer);
//    }
//}
