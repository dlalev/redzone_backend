package com.redzone.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Entity
@Table(name = "users",
        uniqueConstraints = {
                    @UniqueConstraint(columnNames = "username")})
public class Scoreboard {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @NotBlank
        @Size(max = 20)
        private String username;

        @NotBlank
        @Size(max = 255)
        private int score;


        public Scoreboard(){

        }

        public Scoreboard(Long id, @NotBlank @Size(max = 20) String username, @NotBlank @Size(max = 255) int score) {
            this.id = id;
            this.username = username;
            this.score = score;
        }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
