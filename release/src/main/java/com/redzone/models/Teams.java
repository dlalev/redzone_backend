package com.redzone.models;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "teams")
public class Teams {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(length = 20)
    private ETeam teamName;


    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "tu_id", referencedColumnName = "id")
    private List<User> users;

    public Teams(){

    }

    public Teams(ETeam team_name) {
        super();
        this.teamName = team_name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public ETeam getTeam_name() {
        return teamName;
    }

    public void setTeam_name(ETeam team_name) {
        this.teamName = team_name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }


}
