package com.redzone.controllers;

import com.redzone.repositories.TeamsRepository;
import com.redzone.repositories.UserRepository;
import com.redzone.security.jwt.AuthEntryPointJwt;
import com.redzone.security.jwt.JwtUtils;
import com.redzone.security.services.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(TeamsController.class)
class TeamsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TeamsRepository teamRepository;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserDetailsServiceImpl service;

    @MockBean
    private AuthEntryPointJwt authEntryPointJwt;

    @MockBean
    private JwtUtils jwtUtils;


    @Test
    void should_get_All() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/getAllTeams"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void should_get_All_from_BlueTeam() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/blueteamm"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    void should_get_All_from_RedTeam() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/redteamm"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    void addToBlueTeam() throws Exception {


        mockMvc.perform(
                MockMvcRequestBuilders.put("/addtoblue/{id}", 1L))
                .andExpect(MockMvcResultMatchers.status().isOk());


    }

    @Test
    void addToRedTeam() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.put("/addtored/{id}", 1L))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
}