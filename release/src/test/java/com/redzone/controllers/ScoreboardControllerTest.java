package com.redzone.controllers;

import com.redzone.repositories.ScoreboardRepository;
import com.redzone.security.jwt.AuthEntryPointJwt;
import com.redzone.security.jwt.JwtUtils;
import com.redzone.security.services.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(ScoreboardController.class)
class ScoreboardControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ScoreboardRepository scoreboardRepository;

    @MockBean
    private UserDetailsServiceImpl service;

    @MockBean
    private AuthEntryPointJwt authEntryPointJwt;

    @MockBean
    private JwtUtils jwtUtils;

    @Test
    void getUserWithMaxScore() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.get("/topofthemonth"))
                .andExpect(MockMvcResultMatchers.status().isOk());


    }

    @Test
    void getAll() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/scoreboard/getall"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }
}