package com.redzone.controllers;

import com.redzone.repositories.TeamsRepository;
import com.redzone.repositories.UserRepository;
import com.redzone.security.jwt.AuthEntryPointJwt;
import com.redzone.security.jwt.JwtUtils;
import com.redzone.security.services.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(ChatController.class)
class ChatControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsServiceImpl service;

    @MockBean
    private AuthEntryPointJwt authEntryPointJwt;

    @MockBean
    private JwtUtils jwtUtils;

    @Test
    void sendToAll() throws Exception {

        mockMvc.perform(
                MockMvcRequestBuilders.get("/user-all"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}