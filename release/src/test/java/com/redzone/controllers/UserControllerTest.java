package com.redzone.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.redzone.exception.ResourceNotFoundException;
import com.redzone.models.ERole;
import com.redzone.models.User;
import com.redzone.repositories.RoleRepository;
import com.redzone.repositories.UserRepository;
import com.redzone.security.jwt.AuthEntryPointJwt;
import com.redzone.security.jwt.JwtUtils;
import com.redzone.security.services.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.*;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private UserDetailsServiceImpl service;

    @MockBean
    private AuthEntryPointJwt authEntryPointJwt;

    @MockBean
    private JwtUtils jwtUtils;

    @Test
    void should_return_String_with_Public_Access() throws Exception{

        mockMvc.perform(
                MockMvcRequestBuilders.get("/api/getall"))
        .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Public."));
    }

    @Test
    @WithMockUser(roles = "USER")
    void should_return_String_with_User_Access() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/api/user"))
            .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Choose which game to play"));
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void should_return_all_users_with_admin_Access() throws Exception {
        List<User> listUsers = new ArrayList<>();

        listUsers.add(new User("username", "email", "password", 1));
        listUsers.add(new User("username2", "email2", "password2", 12));
        listUsers.add(new User("username3", "email3", "password3", 13));

        Mockito.when(userRepository.findAll()).thenReturn(listUsers);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/admin"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void should_not_find_User_By_Id_And_throw_Not_found() throws Exception {

        when(userRepository.findById(1L)).thenThrow(new ResourceNotFoundException(""));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/admin/user/{id}", 1L))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        verify(userRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void should_find_User_By_Id_As_Admin() throws Exception {

        User first = new User();
            first.setId(1L);
            first.setUsername("username");
            first.setEmail("email");
            first.setPassword("password");
            first.setScore(1);

        when(userRepository.findById(1L)).thenReturn(Optional.of(first));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/admin/user/{id}", 1L))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(1)));

        verify(userRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(userRepository);
    }




//    @Test
//    @WithMockUser(roles = "ADMIN")
//    void should_delete_User() throws Exception {
//
//        User first = new User();
//            first.setId(1L);
//            first.setUsername("username");
//            first.setEmail("email");
//            first.setPassword("password");
//            first.setScore(1);
//
//        User second = new User();
//        second.setId(2L);
//        second.setUsername("second");
//        second.setEmail("second");
//        second.setPassword("second");
//        second.setScore(12);
//
//
//        when(userRepository.findById(1L)).thenReturn(Optional.of(first));
//
//        mockMvc.perform(MockMvcRequestBuilders.delete("/api/admin/user/{id}", 1L))
//                .andExpect(MockMvcResultMatchers.status().isOk());
//    }

    @Test
    void should_update_score() throws Exception {

        User first = new User();
        first.setId(1L);
        first.setUsername("username");
        first.setEmail("email");
        first.setPassword("password");
        first.setScore(1);

        when(userRepository.findById(1L)).thenReturn(Optional.of(first));



        mockMvc.perform(MockMvcRequestBuilders.put("/increasescore/{id}/{score}", 1L, 5))
                .andExpect(MockMvcResultMatchers.status().isOk());


    }

    @Test
    void should_update_role() throws Exception {

        User first = new User();
        first.setId(1L);
        first.setUsername("username");
        first.setEmail("email");
        first.setPassword("password");
        first.setScore(1);

        when(userRepository.findById(1L)).thenReturn(Optional.of(first));



        mockMvc.perform(MockMvcRequestBuilders.put("/updateRole/{id}/{role}", 1L, ERole.ROLE_USER))
                .andExpect(MockMvcResultMatchers.status().isOk());


    }


//    Status 400;
//    @Test
//    @WithMockUser(roles = "ADMIN")
//    void should_update_User_By_Id_As_Admin() throws Exception {
//
//        User first = new User();
//        first.setId(1L);
//        first.setUsername("username");
//        first.setEmail("email");
//        first.setPassword("password");
//        first.setScore(1);
//
//        when(userRepository.findById(1L)).thenReturn(Optional.of(first));
//
//        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
//        String json = ow.writeValueAsString(first);
//
//        mockMvc.perform(MockMvcRequestBuilders.put("/api/admin/updateuser/{id}",1L, first))
//                .andExpect(MockMvcResultMatchers.status().isOk());
////                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
////                .andExpect(MockMvcResultMatchers.jsonPath("$.id", is(1)));
//
////        verify(userRepository, times(1)).findById(1L);
////        verifyNoMoreInteractions(userRepository);
//    }



}