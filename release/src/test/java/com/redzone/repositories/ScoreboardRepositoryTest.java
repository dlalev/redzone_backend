package com.redzone.repositories;

import com.redzone.models.Scoreboard;
import com.redzone.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

class ScoreboardRepositoryTest {

    @Mock
    ScoreboardRepository scoreboardRepository;

    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void should_get_Max_Score() {
        Long id1 = Long.valueOf(3);
        Long id2 = Long.valueOf(4);
        Long id3 = Long.valueOf(5);
        Scoreboard scoreboard = new Scoreboard(id1, "username1", 5);
        Scoreboard scoreboard1 = new Scoreboard(id2, "username2", 10);
        Scoreboard scoreboard2 = new Scoreboard(id3, "username3", 15);

        when(scoreboardRepository.getMaxScore()).thenReturn(scoreboard2.getScore());

        assertEquals(15, scoreboard2.getScore());
    }

    @Test
    @Transactional
    void should_find_Scoreboard_By_Score() {
        Scoreboard scoreboard1 = new Scoreboard();
        scoreboard1.setScore(15);

        when(scoreboardRepository.findByScore(15)).thenReturn(java.util.Optional.of(scoreboard1));

        assertEquals(15, scoreboard1.getScore());
    }


    @Test
    void should_find_Top10_OrderBy_ScoreDesc() {


        Scoreboard scoreboard = new Scoreboard(1L, "username1", 5);
        Scoreboard scoreboard1 = new Scoreboard(2L, "username2", 10);
        Scoreboard scoreboard2 = new Scoreboard(3L, "username3", 15);
        Scoreboard scoreboard3 = new Scoreboard(4L, "username3", 16);
        Scoreboard scoreboard4 = new Scoreboard(5L, "username3", 17);
        Scoreboard scoreboard5 = new Scoreboard(6L, "username3", 18);
        Scoreboard scoreboard6 = new Scoreboard(7L, "username3", 19);
        Scoreboard scoreboard7 = new Scoreboard(8L, "username3", 150);
        Scoreboard scoreboard8 = new Scoreboard(9L, "username3", 155);
        Scoreboard scoreboard9 = new Scoreboard(10L, "username3", 12);

        List<Scoreboard> list = new ArrayList<Scoreboard>();


        list.add(scoreboard);
        list.add(scoreboard1);
        list.add(scoreboard2);
        list.add(scoreboard3);
        list.add(scoreboard4);
        list.add(scoreboard5);
        list.add(scoreboard6);
        list.add(scoreboard7);
        list.add(scoreboard8);
        list.add(scoreboard9);



        when(scoreboardRepository.findTop10ByOrderByScoreDesc()).thenReturn(list);

        assertEquals(10, list.size());

    }
}