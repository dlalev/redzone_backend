package com.redzone.repositories;

import com.redzone.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import org.junit.Assert;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.*;


class UserRepositoryTest {

    @Mock
    UserRepository userRepository;

    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void should_save_User() {
        User user1 = new User("test","test","test",5);

        when(userRepository.save(user1)).thenReturn(user1);

        User savedUser = new User("test","test","test",5);

        assertEquals(savedUser.getId(), user1.getId());
    }

    @Test
    void should_find_User_By_Username() {
        User user1 = new User("test","test","test",5);

        when(userRepository.findByUsername("test")).thenReturn(java.util.Optional.of(user1));

        assertEquals("test", user1.getUsername());
    }

    @Test
    void should_exist_by_Username() {

        User user = new User("test","test","test",5);

        when(userRepository.existsByUsername("test")).thenReturn(true);

        assertEquals(true, userRepository.existsByUsername("test"));
    }

    @Test
    void should_exist_By_Email() {
        User user = new User("test","test","test",5);

        when(userRepository.existsByEmail("test")).thenReturn(true);

        assertEquals(true, userRepository.existsByEmail("test"));
    }

    @Test
    void should_find_all_Users() {
        User user1 = new User("test","test","test",5);
        User user2 = new User("test1","test1","test1",5);
        User user3 = new User("test2","test2","test2",5);

        List<User> list = new ArrayList<User>();

        list.add(user1);
        list.add(user2);
        list.add(user3);

        List<User> founded = new ArrayList<User>();

        when(userRepository.findAll()).thenReturn(list);

        assertEquals(3, list.size());
    }


}