package com.redzone.repositories;

import com.redzone.models.ETeam;
import com.redzone.models.Teams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


class TeamRepositoryTest {

    @Mock
    TeamsRepository teamRepository;

    @BeforeEach
    void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void should_find_Team_By_TeamName_Like() {
        Teams team = new Teams();
        team.setTeam_name(ETeam.BLUE);
        List<Teams> teams = new ArrayList<>();
        teams.add(team);

        when(teamRepository.findByTeamNameLike(ETeam.BLUE) ).thenReturn(teams);

        assertEquals(1, teams.size());
    }

    @Test
    void should_delete_Team_By_Id() {
        Teams team = new Teams(ETeam.BLUE);
        Teams team1 = new Teams(ETeam.RED);

        List<Teams> list = new ArrayList<>();

        list.add(team);
        list.add(team1);

        List<Teams> newList = new ArrayList<>();
        newList = list;



        when(teamRepository.findAll()).thenReturn(list);
        newList.remove(team1);
        teamRepository.deleteById(1L);

        assertEquals(true, newList.contains(team));
    }

}